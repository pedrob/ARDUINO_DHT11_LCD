
#include <LiquidCrystal.h>
LiquidCrystal lcd(12,11,5,4,3,2); //Define lcd PINs

#include "DHT.h" //include sensor library

#define DHTPIN 7 //define sensor PIN
#define DHTTYPE DHT11 //type of used sensor

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  //initializing lcd and display welcome message
  lcd.begin(16,2);
  lcd.setCursor(0,0);
  
  //Welcome message Loop
  char arr[]={'S','t','a','r','t','i','n','g'};
  int arrSize = 8;
  int arrSize2 = 10;
  char arr2[]={'M','a','d','e',' ','b','y','L','E','K'};
  
  for(int i =0; i < 8; i++){
    lcd.setCursor(i,0);
    lcd.print(arr[i]);
    delay(500);
    }
    
    for (int i=0; i<8; i++){
      lcd.print(".");
      delay(500);
      }
      
      for(int i=0; i<10;i++){
        lcd.setCursor(i,1);
        lcd.print(arr2[i]);
        delay(500);
        }
        
        for(int i=0; i<6; i++){
          lcd.print(".");
          delay(500);
          }
          //initialize sensor
          Serial.begin(115200);
          
          }
          void loop() {
            //Defining temperature and humidity as integer values
            int dhtTemp = dht.readTemperature();
            int dhtHum = dht.readHumidity();
            //Start displaying data 
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Temperature: ");
            lcd.setCursor(13,0);
            lcd.print(dhtTemp); //Display temperature
            lcd.print("C");
            lcd.setCursor(0,1);
            lcd.print("Humidity: ");
            lcd.setCursor(13,1);
            lcd.print(dhtHum); //Display humidity
            lcd.print("%");
            delay(200);
            
            }
